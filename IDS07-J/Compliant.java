import java.io.File;

public class Compliant {
	static class DirList {
		  public static void main(String[] args) throws Exception {
		    File dir = new File(System.getProperty("dir"));
		    if (!dir.isDirectory()) {
		      System.out.println("Not a directory");
		    } else {
		      for (String file : dir.list()) {
		        System.out.println(file);
		      }
		    }
		  }
		}
}
