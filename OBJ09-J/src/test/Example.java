package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Example {

	@Test
	void noncompliant() {
		Car car = new Car();
		assertTrue(car.getClass().getName().equals("test.Car"));
	}

	@Test
	void compliant() {
		Car car = new Car();
		assertTrue(car.getClass() == Car.class);
	}

}
