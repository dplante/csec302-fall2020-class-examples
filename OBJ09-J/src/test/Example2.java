package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Example2 {

	@Test
	void noncompliant() {
		Car car1 = new Car();
		Car car2 = new Car();
		assertTrue(car1.getClass().getName().equals(car2.getClass().getName()));
	}
	
	@Test
	void complaint() {
		Car car1 = new Car();
		Car car2 = new Car();
		assertTrue(car1.getClass() == car2.getClass());
	}

}
