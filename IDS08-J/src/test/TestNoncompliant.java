package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import main.*;


public class TestNoncompliant {
	
	@Before
	public void SetUp() throws Exception{
		
	}
	
	
	@Test
	public void test1() {
		regexmatching(".*)|(.*");
	}
	
	
	@Test
	public void test2() {
		regexmatching("");
	}
	
	@Test
	public void test3() {
		regexmatching("@#$%^&*");
	}
	
	@Test
	public void test4() {
		regexmatching("private");
	}
	
	@Test
	public void test5() {
		regexmatching("10:47:03");
	}
	
	@Test
	public void test6() {
		regexmatching("10:47:43");
	}
	
	@Test
	public void test7() {
		regexmatching("423");
	}
	
	private void regexmatching(String s) {
		Noncompliant typing = new Noncompliant();
		typing.FindLogEntry(s);
		assertTrue(typing.matches.isEmpty());
	}


}
