package test;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import main.*;
public class TestComplicant {

	@Before
	public void SetUp() throws Exception{
		
	}
	
	
	@Test
	public void test1() {
		regexmatchingWhiteListing(".*)|(.*");
		regexmatchingPatternQuote(".*)|(.*");
	}
	
	
//	@Test
//	public void test2() {
//		regexmatchingWhiteListing("");
//		regexmatchingPatternQuote("");
//	}
	
	@Test
	public void test3() {
		regexmatchingWhiteListing("@#$%^&*");
		regexmatchingPatternQuote("@#$%^&*");
	}
	
	@Test
	public void test4() {
		regexmatchingWhiteListing("private");
		regexmatchingPatternQuote("private");
	}
	
	@Test
	public void test5() {
		regexmatchingWhiteListing("10:47:03");
		regexmatchingPatternQuote("10:47:03");
	}
	
	@Test
	public void test6() {
		regexmatchingWhiteListing("10:47:43");
		regexmatchingPatternQuote("10:47:43");
	}
	
	@Test
	public void test7() {
		regexmatchingWhiteListing("423");
		regexmatchingPatternQuote("423");
	}
	
	private void regexmatchingWhiteListing(String s) {
		Compliant typing = new Compliant();
		typing.FindLogEntry1(s);
		assertTrue(typing.matches.isEmpty());
	}
	
	private void regexmatchingPatternQuote(String s) {
		Compliant typing = new Compliant();
		typing.FindLogEntry2(s);
		assertTrue(typing.matches.isEmpty());
	}


}
