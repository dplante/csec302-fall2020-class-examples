package main;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
public class Compliant {
	
	public Vector<String> matches = new Vector();
	
	//White listing
	private String whitelisting(String s) {
		 // Sanitize search string
	    StringBuilder sb = new StringBuilder(s.length());
	    for (int i = 0; i < s.length(); ++i) {
	        char ch = s.charAt(i);
	        if (Character.isLetterOrDigit(ch) || ch == ' ' || ch == '\'') {
	            sb.append(ch);
	        }
	    }
	    s = sb.toString();
	    return s;
	}
	
	//Pattern.quote()
	private String patternQuote(String search) {
		search = Pattern.quote(search);
		return search;
	}
	
	public void FindLogEntry1(String search) {
		String s = whitelisting(search);
		FindLogEntry(s);
	}
	
	public void FindLogEntry2(String search) {
		String s = patternQuote(search);
		FindLogEntry(s);
	}
	
	
    private  void FindLogEntry(String search) {
    	if(search.isEmpty()) {
    		return;
    	}
        // Construct regex dynamically from user string
        String regex = "(.*? +public\\[\\d+\\] +.*" + search + ".*)";
        Pattern searchPattern = Pattern.compile(regex);
        try (FileInputStream fis = new FileInputStream("C:\\Users\\Azer\\Documents\\AZ\\2020_Fall\\IDS08-J\\src\\log.txt")) {
            FileChannel channel = fis.getChannel();
            // Get the file's size and map it into memory
            long size = channel.size();
            final MappedByteBuffer mappedBuffer = channel.map(
                    FileChannel.MapMode.READ_ONLY, 0, size);
            Charset charset = Charset.forName("ISO-8859-15");
            final CharsetDecoder decoder = charset.newDecoder();
            // Read file into char buffer
            CharBuffer log = decoder.decode(mappedBuffer);
            Matcher logMatcher = searchPattern.matcher(log);
            while (logMatcher.find()) {
                String match = logMatcher.group();
                if (!match.isEmpty()) {
                	matches.add(search);
                    System.out.println(match);
                }
            }
        } catch (IOException ex) {
        	matches.add("");
            System.err.println("thrown exception: " + ex.toString());
            Throwable[] suppressed = ex.getSuppressed();
            for (int i = 0; i < suppressed.length; i++) {
                System.err.println("suppressed exception: "
                        + suppressed[i].toString());
            }
        }
        return;
    }
}
