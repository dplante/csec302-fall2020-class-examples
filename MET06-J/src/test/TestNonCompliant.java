package test;

import static org.junit.Assert.assertNull;

import java.net.HttpCookie;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.NonCompliant;
import main.SubNonCompliant;

class TestNonCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() throws CloneNotSupportedException {
		HttpCookie[] hc = new HttpCookie[20];
	    for (int i = 0 ; i < hc.length; i++){
	      hc[i] = new HttpCookie("cookie" + i,"" + i);
	    }
	    NonCompliant nc = new SubNonCompliant(hc);
	    NonCompliant ncClone = (NonCompliant) nc.clone();
	    
	    HttpCookie[] clonedCookies = ncClone.getCookies();
	    assertNull(clonedCookies[0].getDomain());
	   
	}

}
