package test;

import static org.junit.Assert.assertNull;

import java.net.HttpCookie;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;
import main.SubCompliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void test() throws CloneNotSupportedException {
		HttpCookie[] hc = new HttpCookie[20];
	    for (int i = 0 ; i < hc.length; i++){
	      hc[i] = new HttpCookie("cookie" + i,"" + i);
	    }
	    Compliant c = new SubCompliant(hc);
	    Compliant cClone = (Compliant) c.clone();
	    
	    HttpCookie[] clonedCookies = cClone.getCookies();
	    assertNull(clonedCookies[0].getDomain());
	   
	}


}
