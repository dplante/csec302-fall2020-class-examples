package main;

import java.net.HttpCookie;

public class NonCompliant implements Cloneable {
	HttpCookie[] cookies;

	NonCompliant(HttpCookie[] c) {
		cookies = c;
	}

	public Object clone() throws CloneNotSupportedException {
		final NonCompliant clone = (NonCompliant) super.clone();
		clone.doSomething(); // Invokes overridable method
		clone.cookies = clone.deepCopy();
		return clone;
	}

	void doSomething() { // Overridable
		System.out.println("doSomething called on Super class");
		for (int i = 0; i < cookies.length; i++) {
			cookies[i].setValue("" + i);
		}
	}

	HttpCookie[] deepCopy() {
		if (cookies == null) {
			throw new NullPointerException();
		}

		// Deep copy
		HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];

		for (int i = 0; i < cookies.length; i++) {
			// Manually create a copy of each element in array
			cookiesCopy[i] = (HttpCookie) cookies[i].clone();
		}
		return cookiesCopy;
	}
	
	public HttpCookie[] getCookies() {
		return cookies;
	}
}

