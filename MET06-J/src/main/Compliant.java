package main;

import java.net.HttpCookie;

public class Compliant  implements Cloneable {
	HttpCookie[] cookies;

	Compliant(HttpCookie[] c) {
		cookies = c;
	}

	public Object clone() throws CloneNotSupportedException {
		final Compliant clone = (Compliant) super.clone();
		clone.doSomething(); // Invokes overridable method
		clone.cookies = clone.deepCopy();
		return clone;
	}

	final void doSomething() { // Overridable
		System.out.println("doSomething called on Super class");
		for (int i = 0; i < cookies.length; i++) {
			cookies[i].setValue("" + i);
		}
	}

	final HttpCookie[] deepCopy() {
		if (cookies == null) {
			throw new NullPointerException();
		}

		// Deep copy
		HttpCookie[] cookiesCopy = new HttpCookie[cookies.length];

		for (int i = 0; i < cookies.length; i++) {
			// Manually create a copy of each element in array
			cookiesCopy[i] = (HttpCookie) cookies[i].clone();
		}
		return cookiesCopy;
	}
	
	public HttpCookie[] getCookies() {
		return cookies;
	}
}
