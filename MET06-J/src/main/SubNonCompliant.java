package main;

import java.net.HttpCookie;

public class SubNonCompliant extends NonCompliant {

	public SubNonCompliant(HttpCookie[] c) {
		super(c);
	}

	public Object clone() throws CloneNotSupportedException {
		final SubNonCompliant clone = (SubNonCompliant) super.clone();
		
		return clone;
	}

	void doSomething() { // Erroneously executed
		System.out.println("doSomething called on Subclass");
		for (int i = 0;i < cookies.length; i++) {
			cookies[i].setDomain(i + ".foo.com");
		}
	}
}
