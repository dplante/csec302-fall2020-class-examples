package main;

import java.net.HttpCookie;

public class SubCompliant extends Compliant {
	public SubCompliant(HttpCookie[] c) {
		super(c);
	}

	public Object clone() throws CloneNotSupportedException {
		final SubCompliant clone = (SubCompliant) super.clone();
		clone.doSomething();
		return clone;
	}

	
}
