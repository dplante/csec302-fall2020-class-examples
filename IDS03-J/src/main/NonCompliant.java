package main;

import java.util.logging.Logger;

public class NonCompliant {
	Logger logger;
	
	public NonCompliant() {
		this.logger = Logger.getAnonymousLogger();
	}
	
	public String logAuthResult(String username, boolean isLoginSuccessful) {
		String msg = "";
		if(isLoginSuccessful) {
			msg = "User login succeeded for: ";
		}
		else {
			msg = "User login failed for: ";
		}
		this.logger.severe(msg + username);
		
		return username;
	}
}
