package main;

import java.util.logging.Logger;
import java.util.regex.Pattern;

public class Compliant {
	Logger logger;
	
	public Compliant() {
		this.logger = Logger.getAnonymousLogger();
	}
	
	public String logAuthResult(String username, boolean isLoginSuccessful) {
		String msg = "";
		if(isLoginSuccessful) {
			msg = "User login succeeded for: ";
		}
		else {
			msg = "User login failed for: ";
		}
		
		String sanitizedUsername = this.sanitizeUser(username);
		this.logger.severe(msg + sanitizedUsername);
		
		return sanitizedUsername;	
	}
	
	public String sanitizeUser(String username) {
	  return Pattern.matches("[A-Za-z0-9_]+", username)
	      ? username : "unauthorized user";
	}

	public void sanitizedLoggingOfAuthResult(String username, boolean isLoginSuccessful) {
		SanitizedTextLogger sanitizedTextLogger = new SanitizedTextLogger(this.logger);
		String msg = "";
		if(isLoginSuccessful) {
			msg = "User login succeeded for: ";
		}
		else {
			msg = "User login failed for: ";
		}
		
		sanitizedTextLogger.severe(msg + username);
	}
}
