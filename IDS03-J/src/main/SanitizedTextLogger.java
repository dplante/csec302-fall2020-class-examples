package main;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SanitizedTextLogger extends Logger {

	Logger delegate;
	public SanitizedTextLogger(Logger delegate) {
		super(delegate.getName(), delegate.getResourceBundleName());
		this.delegate = delegate;
	}
	
	public String sanitize(String msg) {
	    Pattern newline = Pattern.compile("\n");
	    Matcher matcher = newline.matcher(msg);
	    return matcher.replaceAll("\n  ");
	}

	  public void severe(String msg) {
	    delegate.severe(sanitize(msg));
	  }
}
