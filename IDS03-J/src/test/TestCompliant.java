package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void loginSuccessCRLF() {
		this.catchIt(
			"guest\r\n" + 
			"Aug 29, 2020 2:05:31 PM main.Compliant logAuthResult\r\n" + 
			"SEVERE: User login succeeded for: administrator", 
			true,
			false
		);
	}

	@Test
	void loginFailedCRLF() {
		this.catchIt(
			"guest\r\n" + 
			"Aug 29, 2020 2:05:31 PM main.Compliant logAuthResult\r\n" + 
			"SEVERE: User login succeeded for: administrator", 
			false,
			false
		);
	}
	
	@Test
	void loginFailedLF() {
		this.catchIt(
			"guest\n" + 
			"Aug 29, 2020 2:05:31 PM main.Compliant logAuthResult\n" + 
			"SEVERE: User login succeeded for: administrator", 
			false,
			false
		);
	}
	
	@Test
	void loginSuccessValidInput() {
		this.catchIt("administrator", true, true);
	}
	
	@Test
	void logSanitizedText() {
		Compliant compliant = new Compliant();
		compliant.sanitizedLoggingOfAuthResult(
			"guest\n" + 
			"Aug 29, 2020 2:05:31 PM main.Compliant logAuthResult\n" + 
			"SEVERE: User login succeeded for: administrator", 
			false
		);
	}
	
	void catchIt(String username, boolean isLoginSuccessful, boolean isValidInput) {
		
		Compliant compliant = new Compliant();
		String loggedUsername = compliant.logAuthResult(username, isLoginSuccessful);
		if(isValidInput) {
			// Passed username should match with returned value to pass the test
			assertTrue(loggedUsername.equals(username));
		}
		else {
			// Passed username should not match with returned value to pass the test
			assertFalse(loggedUsername.equals(username));
		}
		
	}
}
