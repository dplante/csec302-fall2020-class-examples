package main;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class Compliant {
    private static void receiveXMLStream(InputStream inStream,
                                         DefaultHandler defaultHandler) throws ParserConfigurationException,
            SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        // Create an XML reader to set the entity resolver.
        XMLReader reader = saxParser.getXMLReader();

        // OWASP'S Reccomendation, most secure option: Disable External Entities.
        // reader.setFeature("http://xml.org/sax/features/external-general-entities", false);

        reader.setEntityResolver(new CustomResolver());
        reader.setContentHandler(defaultHandler);

        InputSource is = new InputSource(inStream);
        reader.parse(is);
    }

    public static void main(String[] args) throws ParserConfigurationException,
            SAXException, IOException {
        try {
            receiveXMLStream(new FileInputStream("/home/cilardo/class/csec302/csec302-fall20-assign-02/IDS17-J/evil.xml"), new DefaultHandler());
        } catch (java.net.MalformedURLException mue) {
            System.err.println("Malformed URL Exception: " + mue);
        }
    }
}