package main;
import java.io.IOException;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

class CustomResolver implements EntityResolver {
    public InputSource resolveEntity(String publicId, String systemId)
            throws SAXException, IOException {

        // Check for known good entities
        String entityPath = "/home/cilardo/class/csec302/csec302-fall20-assign-02/IDS17-J/good.xml";
        if (systemId.equals(entityPath)) {
            System.out.println("Resolving entity: " + publicId + " " + systemId);
            return new InputSource(entityPath);
        } else {
            // Disallow unknown entities by returning a blank path
            System.out.println("Unknown entity, returning a blank path");
            return new InputSource();
        }
    }
}