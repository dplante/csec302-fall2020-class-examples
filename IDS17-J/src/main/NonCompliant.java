package main;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class NonCompliant {

    private static void receiveXMLStream(InputStream inStream, DefaultHandler defaultHandler)
            throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(inStream, defaultHandler);
    }

    public static void main(String[] args) throws ParserConfigurationException,
            SAXException, IOException {
        try {
            System.out.println("About to print important info...");
            receiveXMLStream(new FileInputStream("/home/cilardo/class/csec302/csec302-fall20-assign-02/IDS17-J/evil.xml"), new DefaultHandler());

            System.out.println("IMPORTANT INFO!!!");
        } catch (java.net.MalformedURLException mue) {
            System.err.println("Malformed URL Exception: " + mue);
        }
    }
}

// PORTSWIGGER XML Example
// https://portswigger.net/web-security/xxe/lab-exploiting-xxe-to-retrieve-files
//<!DOCTYPE test [ <!ENTITY xxe SYSTEM "file:///etc/passwd"> ]>
//        Then replace the productId number with a reference to the external entity: &xxe;
