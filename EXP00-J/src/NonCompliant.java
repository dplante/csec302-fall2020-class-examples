//https://en.wikipedia.org/wiki/Time-of-check_to_time-of-use

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NonCompliant {

    // ALSO NON COMPLIANT BECAUSE OF TOC, TOU issues.
    public static void deleteFile() throws IOException {

        File someFile = new File("useless");
        someFile.delete();

        // Here an attacker creates a file that is a sym link to an important writeable file

        FileWriter fr = new FileWriter(someFile, true);
        fr.write("data");
        fr.close();
    }

    public static void main(String[] args) throws IOException {

        deleteFile();

    }
}
