package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testFileDeletion() {
		// Testing the deletion of some non existing file
		Compliant compliant = new Compliant();
		
		String filename = "some-non-existing-file.txt";
		File file = new File(filename);
		boolean isExist = file.exists();
		
		boolean result = compliant.deleteFile("someFileName.txt");
		if(isExist == false && result == true) {
			fail("Cannot delete non existing file");
		}
	}
	
	@Test
	void testStringReplacement() {
		// Checking whether the string is correctly replaced
		String original = "insecure";
		String expected = "9nsecure";
		
		Compliant compliant = new Compliant();
		String result = compliant.replaceString(original, 'i', '9');
		System.out.println("Replaced string: " + result);
		if(result.equals(expected) == false) {
			fail("String is not replaced");
		}
	}
	
	@Test
	void testStringConcatenation() {
		// Checking whether the string is correctly concatenated
		String original = "insecure";
		String suffix = " is now secured.";
		
		String expected = "insecure is now secured.";
		
		Compliant compliant = new Compliant();
		String result = compliant.appendString(original, suffix);
		System.out.println("Concatenated string: " + result);
		if(result.equals(expected) == false) {
			fail("String is not concatenated");
		}
	}

}
