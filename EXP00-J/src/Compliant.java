import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.System.exit;

public class Compliant {

    public static void deleteFile() {

        File someFile;
        someFile = new File("useless/");
        System.out.println(someFile.toString());

        if (!someFile.delete()) {
            System.out.println("Failed to delete  " + someFile.toString());
        }
    }

    public static void main(String[] args) throws IOException {

        deleteFile();

    }
}
