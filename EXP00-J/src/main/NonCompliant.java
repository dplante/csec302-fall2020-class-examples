package main;

import java.io.File;

public class NonCompliant {

	public boolean deleteFile(String filename) {
		File file = new File(filename);
		file.delete();
		return true;
	}
	
	public String replaceString(String original, char oldChar, char newChar) {
		original.replace(oldChar, newChar);
		return original;
	}
	
	public String appendString(String original, String suffix) {
		original.concat(suffix);
		return original;
	}
}
