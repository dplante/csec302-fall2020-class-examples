package main;

import java.io.File;

public class Compliant {

	public boolean deleteFile(String filename) {
		File file = new File(filename);
		boolean isDeleted = file.delete();
		if(isDeleted) {
			System.out.println(filename + "deleted successfully");
		}
		else {
			System.out.println(filename + " is not deleted");
		}
		
		return isDeleted;
	}
	
	public String replaceString(String original, char oldChar, char newChar) {
		return original.replace(oldChar, newChar);
	}
	
	public String appendString(String original, String suffix) {
		return original.concat(suffix);
	}
	
}
