package xmlinjection;

import java.io.BufferedOutputStream;
import java.io.IOException;

public class CompliantOnlineStore {
	public static void createXMLStream(final BufferedOutputStream outStream, final String quantity) throws IOException {
		// Write XML string only if quantity is an unsigned integer (count).
		try {
			int count = Integer.parseUnsignedInt(quantity);
			String xmlString = "<item>\n<description>Widget</description>\n" + "<price>500</price>\n" + "<quantity>"
					+ count + "</quantity></item>";
			outStream.write(xmlString.getBytes());
			outStream.flush();
		} catch (Exception e) {
			throw new NumberFormatException("Invalid quantity");
		}
	}
}
