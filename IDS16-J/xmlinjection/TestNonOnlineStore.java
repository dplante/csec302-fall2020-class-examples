package xmlinjection;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.junit.jupiter.api.Test;

class TestNonOnlineStore {

	@Test
	void validQuantity() {
		try {
			File yourFile = new File("output.txt");
			yourFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(yourFile);
			BufferedOutputStream outStream = new BufferedOutputStream(fout);
			NonOnlineStore.createXMLStreamBad(outStream, "5");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void invalidQuantity() {
		try {
			File yourFile = new File("output.txt");
			yourFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(yourFile);
			BufferedOutputStream outStream = new BufferedOutputStream(fout);
			NonOnlineStore.createXMLStreamBad(outStream, "5adf");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
