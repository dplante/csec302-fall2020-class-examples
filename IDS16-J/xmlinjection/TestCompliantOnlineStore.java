package xmlinjection;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.junit.jupiter.api.Test;

class TestCompliantOnlineStore {

	@Test
	void validQuantity() {
		try {
			File yourFile = new File("output.txt");
			yourFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(yourFile);
			BufferedOutputStream outStream = new BufferedOutputStream(fout);
			final Exception exception = assertThrows(NumberFormatException.class, () -> {
				CompliantOnlineStore.createXMLStream(outStream, "5");
			});
			final String expectedMessage = "Invalid quantity";
			final String actualMessage = exception.getMessage();

			assertTrue(actualMessage.contains(expectedMessage));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void invalidQuantity() {
		try {
			File yourFile = new File("output.txt");
			yourFile.createNewFile();
			FileOutputStream fout = new FileOutputStream(yourFile);
			BufferedOutputStream outStream = new BufferedOutputStream(fout);
			final Exception exception = assertThrows(NumberFormatException.class, () -> {
				CompliantOnlineStore.createXMLStream(outStream, "5sdf");
			});
			final String expectedMessage = "Invalid quantity";
			final String actualMessage = exception.getMessage();

			assertTrue(actualMessage.contains(expectedMessage));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
