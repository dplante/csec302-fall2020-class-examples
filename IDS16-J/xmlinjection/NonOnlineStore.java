package xmlinjection;

import java.io.BufferedOutputStream;
import java.io.IOException;

public class NonOnlineStore {
	public static void createXMLStreamBad(final BufferedOutputStream outStream, final String quantity)
			throws IOException {
		String xmlString = "<item>\n<description>Widget</description>\n" + "<price>500</price>\n" + "<quantity>"
				+ quantity + "</quantity></item>";
		outStream.write(xmlString.getBytes());
		outStream.flush();
	}
}
