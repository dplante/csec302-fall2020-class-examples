package test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testForUnencodedScriptTag() {
		// Should pass as it replace the noncharacter codepoint with Unicode U+FFFD
		String strWithScript = "This string contains <scr" + "\uFDEF" + "ipt> tag";
		this.catchIt(strWithScript);
	}
	
	
	
	@Test
	void testForEncodedScriptTag() {
		// Should pass as it replace the noncharacter codepoint with Unicode U+FFFD
		String strWithScript = "This string contains " + "\uFE64" + "scr" + "\uFDEF" + "ipt" + "\uFE65" + " tag";
		this.catchIt(strWithScript);
	}

	
	
	@Test
	void testScriptTag() {
		// Should throw an IllegalArgumentException exception
		this.catchIt("<script>");
	}
	
	
	
	void catchIt(String str) {

		try {
			Compliant compliant = new Compliant();
			String filteredString = compliant.filterString(str);
			System.out.println("Filtered String: " + filteredString);
			assertTrue(filteredString.contains("\uFFFD"));
		}
		catch (IllegalArgumentException exception) {
			// Invalid input in string handled if message contains "Invalid input"
			String exceptionMessage = exception.getMessage();
			String expectedMessage = "Invalid input";
			System.out.println(exceptionMessage);
			assertTrue(exceptionMessage.contains(expectedMessage));
		}
		catch (Exception exception) {
			fail(exception.getMessage());
		}
		
	}
}
