package test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.NonCompliant;

class TestNonCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testForUnencodedScriptTag() {
		// Should fail as it removes the noncharacter codepoint
		String strWithScript = "This string contains <scr" + "\uFDEF" + "ipt> tag";
		this.catchIt(strWithScript);
	}
	
	
	
	@Test
	void testForEncodedScriptTag() {
		// Should fail as it removes the noncharacter codepoint
		String strWithScript = "This string contains " + "\uFE64" + "scr" + "\uFDEF" + "ipt" + "\uFE65" + " tag";
		this.catchIt(strWithScript);
	}
	
	
	
	@Test
	void testScriptTag() {
		// Should throw an IllegalArgumentException exception
		this.catchIt("<script>");
	}

	
	
	void catchIt(String str) {

		try {
			NonCompliant nonCompliant = new NonCompliant();
			String filteredString = nonCompliant.filterString(str);
			System.out.println("Filtered String: " + filteredString);
			assertTrue(filteredString.contains("\uFFFD"));
		}
		catch (IllegalArgumentException exception) {
			// Invalid input in string handled if message contains "Invalid input"
			String exceptionMessage = exception.getMessage();
			String expectedMessage = "Invalid input";
			System.out.println(exceptionMessage);
			assertTrue(exceptionMessage.contains(expectedMessage));
		}
		catch (Exception exception) {
			fail(exception.getMessage());
		}
		
	}
}
