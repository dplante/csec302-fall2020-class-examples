package main;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NonCompliant {

	public String filterString(String str) {

		String normalizedString = Normalizer.normalize(str, Form.NFKC);

		
		
		// Validate input
		Pattern pattern = Pattern.compile("<script>");
		Matcher matcher = pattern.matcher(normalizedString);
		if (matcher.find()) {
			// String contains <script> tag which is not allowed
			throw new IllegalArgumentException("Invalid input");
		}

		
		
		// Deletes noncharacter code points Cn
		String filteredString = normalizedString.replaceAll("[\\p{Cn}]", "");
		return filteredString;

	}
	
	
}
