package main;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compliant {

	public String filterString(String str) {

		// 1. Normalize the user input first
		String normalizedString = Normalizer.normalize(str, Form.NFKC);

		
		
		// 2. Replace all noncharacter code points with Unicode U+FFFD
		String filteredString = normalizedString.replaceAll("[\\p{Cn}]", "\uFFFD");

		
		
		// 3. Validate modified input
		Pattern pattern = Pattern.compile("<script>");
		Matcher matcher = pattern.matcher(filteredString);
		if (matcher.find()) {
			// String contains <script> tag which is not allowed
			throw new IllegalArgumentException("Invalid input");
		}

		return filteredString;

	}
}
