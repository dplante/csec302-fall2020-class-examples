package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Compliant {

	//total is private so it cannot be altered from outside
	private int total;

	// hm is private so even if it is mutable field it cannot be altered from outside
	private static final HashMap<Integer, String> hm = new HashMap<Integer, String>();

	// private mutable arrays elements cannot be accessed outside the class
	private static final String[] items = {"str1", "str2"};
	
	// final immutable list is safe for public access as neither its reference nor its value can be modified 
	public static final List<String> itemsList =
			  Collections.unmodifiableList(Arrays.asList(items));
	
	public int getTotal() {
		return total;
	}
	
	void add() {
		if(total < Integer.MAX_VALUE) {
			total++;
		}
		else {
			throw new ArithmeticException("Overflow");
		}
	}

	void remove() {
		if(total > 0) {
			total--;
		}
		else {
			throw new ArithmeticException("Overflow");
		}
	}
	
	public static String getElement(int key) {
	  return hm.get(key);
	}
	
	public static final String getItem(int index) {
	  return items[index];
	}
	 
	public static final int getItemCount() {
	  return items.length;
	}
	
	public static final String[] getItems() {
	  return items.clone();
	}

}
