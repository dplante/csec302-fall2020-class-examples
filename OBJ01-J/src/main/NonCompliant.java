package main;

import java.util.HashMap;

public class NonCompliant {

	// total can be altered outside this class
	public int total;
	
	// hm is public mutable final field, it's elements can still be altered from outside
	public static final HashMap<Integer, String> hm = new HashMap<Integer, String>();
	
	// public mutable arrays elements can be modified outside the class
	public static final String[] items = {"str1", "str2"};
	
	void add() {
		if(total < Integer.MAX_VALUE) {
			total++;
		}
		else {
			throw new ArithmeticException("Overflow");
		}
	}
	
	void remove() {
		if(total > 0) {
			total--;
		}
		else {
			throw new ArithmeticException("Overflow");
		}
	}
 }
