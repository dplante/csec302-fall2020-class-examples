package test;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;
import main.NonCompliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testPrimitiveFieldAlteration() {
		Compliant c = new Compliant();
		int initialVal = c.getTotal();
		// Uncommenting below statement will raise compilation errors as private fields cannot be accessed outside the class
		// c.total = 5; 
		System.out.println("Initial total: " + initialVal + " final total: " + c.getTotal());
		assertTrue(c.getTotal() == initialVal);
	}

	@Test
	void testMutableFieldAlteration() {
		// Uncommenting below statement will raise compilation errors as private fields cannot be accessed outside the class
		// Compliant.hm.put(1, "two");
		System.out.println("hm value: " + NonCompliant.hm.get(1));
		assertTrue(NonCompliant.hm.get(1) == null);
	}
	
	@Test
	void testFinalMutableArrayFieldAlteration() {
		String initialVal = Compliant.getItem(1);
		// Uncommenting below statement will raise compilation errors as private fields cannot be accessed outside the class
		// Compliant.items[1] = "str3";
		System.out.println("Initial total: " + initialVal + " modified val: " +  Compliant.getItem(1));
		assertTrue(Compliant.getItem(1).equals(initialVal));
	}
	
	@Test
	void testModificationOfClonedArray() {
		String initialVal = Compliant.getItem(1);
		Compliant.getItems()[1] = "str3";
		System.out.println("Initial total: " + initialVal + " modified val: " +  Compliant.getItem(1));
		assertTrue(Compliant.getItem(1).equals(initialVal));
	}
	
	@Test
	void testModificationOfImmutableArray() {
		Exception exception = assertThrows(UnsupportedOperationException.class, () -> {
			Compliant.itemsList.add("test");
		});
		
		if(exception.getClass() == UnsupportedOperationException.class) {
			System.out.println("Cannot modify immutable list");
		}
		
	}
}
