package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.NonCompliant;

class TestNonCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testPrimitiveFieldAlteration() {
		NonCompliant nc = new NonCompliant();
		int initialVal = nc.total;
		nc.total = 5;
		System.out.println("Initial total: " + initialVal + " final total: " + nc.total);
		assertTrue(nc.total == initialVal);
	}

	@Test
	void testMutableFieldAlteration() {
		NonCompliant.hm.put(1, "two");
		System.out.println("hm value: " + NonCompliant.hm.get(1));
		assertTrue(NonCompliant.hm.get(1) == null);
	}
	
	@Test
	void testFinalMutableArrayFieldAlteration() {
		String initialVal = NonCompliant.items[1];
		NonCompliant.items[1] = "str3";
		System.out.println("Initial total: " + initialVal + " modified val: " +  NonCompliant.items[1]);
		assertTrue(NonCompliant.items[1].equals(initialVal));
	}
}
