package main;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Compliant {
	private static final String[] publicStrings = {"hello", "public", "world"};
	public static final List<String> publicStringsList = Collections.unmodifiableList(Arrays.asList(publicStrings));
	
	private static final String[] privateStrings = {"hello", "private", "world"};
	
	public static final String[] getPrivateStrings() {
		return privateStrings.clone();
	}
}
