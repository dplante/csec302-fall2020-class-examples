package main;

public class NonCompliant {
	public static final String[] publicStrings = {"hello", "public", "world"};
	private static final String[] privateStrings = {"hello", "private", "world"};
	
	public static final String[] getPrivateStrings() {
		return privateStrings;
	}
}
