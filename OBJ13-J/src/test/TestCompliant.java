package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;

class TestCompliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testModificationOfPublicArray() {
		Exception ex = assertThrows(UnsupportedOperationException.class, () -> {
			Compliant.publicStringsList.set(2, "Java");
		});
		
		assertEquals(ex.getClass(), UnsupportedOperationException.class);
	}
	
	@Test
	void testModificationOfPrivateArray() {
		String original = Compliant.getPrivateStrings()[2];
		Compliant.getPrivateStrings()[2] = "Java";
		
		assertEquals(original, Compliant.getPrivateStrings()[2]);
	}

}
