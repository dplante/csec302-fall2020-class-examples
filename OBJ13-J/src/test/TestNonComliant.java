package test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.NonCompliant;

class TestNonComliant {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testModificationOfPublicArray() {
		String original = NonCompliant.publicStrings[2];
		NonCompliant.publicStrings[2] = "Java";
		
		assertEquals(original, NonCompliant.publicStrings[2]);
	}
	
	@Test
	void testModificationOfPrivateArray() {
		String original = NonCompliant.getPrivateStrings()[2];
		NonCompliant.getPrivateStrings()[2] = "Java";
		
		assertEquals(original, NonCompliant.getPrivateStrings()[2]);
	}

}
