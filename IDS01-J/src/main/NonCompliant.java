package main;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NonCompliant {

	public String convert(String s) {
			 
		// Validate
		Pattern pattern = Pattern.compile("[<>]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
		    // Found blacklisted tag
			throw new IllegalStateException("Improper string");
		}
		
		// Normalize
		s = Normalizer.normalize(s, Form.NFKC);
		
		return s;

	}

}
