package main;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compliant {

	public String convert(String s) {
		// Normalize
		s = Normalizer.normalize(s, Form.NFKC);
		 
		// Validate
		Pattern pattern = Pattern.compile("[<>]");
		Matcher matcher = pattern.matcher(s);
		if (matcher.find()) {
		    // Found blacklisted tag
			throw new IllegalStateException();
		}
		return s;

	}

}
