package test;
/**
 * Test non-compliant code to catch script tags
 * 
 * See: https://www.codejava.net/testing/junit-test-exception-examples-how-to-assert-an-exception-is-thrown
 * 
 * Note that as we saw in class, this solution is NOT complete.  
 * It is part of a bigger strategy in filtering untrusted user input.
 */

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import main.Compliant;
import main.NonCompliant;

class TestNonCompliant {
	private NonCompliant compliant;
	
	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void unencodedBrackets() {
		this.catchIt("<script>");
	}
	
	@Test 
	void encodedBrackets() {
		this.catchIt("\uFE64" + "script" + "\uFE65");
	}
	
	@Test
	void fullyEncoded() {
		this.catchIt("\uFE64\u0053\u0063\u0052\u0069\u0070\u0074\uFE65");
	}
	
	void catchIt(String s) {
		Exception exception = assertThrows(IllegalStateException.class, () -> {
	        NonCompliant compliant = new NonCompliant();
	        compliant.convert(s);
	    });
	 
	    String expectedMessage = "Improper string";
	    String actualMessage = exception.getMessage();
	    System.out.println(actualMessage);
	 
	    assertTrue(actualMessage.contains(expectedMessage));
	}

}
