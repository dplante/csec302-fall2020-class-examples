package main;
public class BankOperations {
	//public final void finalize() {
		// do nothing...
	//}
	
	public BankOperations() {
		if (!performSSNVerification()) {
			throw new SecurityException("Access Denied!");
		}
	}

	private boolean performSSNVerification() {
		return false; // Returns true if data entered is valid, else false
		// Assume that the attacker always enters an invalid SSN
	}
	
	 /*public BankOperations() {
		    this(performSSNVerification());
		  }
		 
		  private BankOperations(boolean secure) {
		    // secure is always true
		    // Constructor without any security checks
		  }
		 
		  private static boolean performSSNVerification() {
		    // Returns true if data entered is valid, else throws a SecurityException
		    // Assume that the attacker just enters invalid SSN, so this method always throws the exception
		    throw new SecurityException("Invalid SSN!");
		  }
	
	public void greet() {
		System.out.println("Welcome user! You may now use all the features.");
	}*/
	
}
/*
class BankOperations {
	  private volatile boolean initialized = false;
	 
	  public BankOperations() {
	    if (!performSSNVerification()) {
	      return;                // object construction failed
	    }
	 
	    this.initialized = true; // Object construction successful
	  }
	 
	  private boolean performSSNVerification() {
	    return false;
	  }
	 
	  public void greet() {
	    if (!this.initialized) {
	      throw new SecurityException("Invalid SSN!");
	    }
	 
	    System.out.println(
	        "Welcome user! You may now use all the features.");
	  }
	}
*/